package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.sun.org.apache.bcel.internal.generic.NEW;
import model.data_structures.MaxHeapCP;
import model.data_structures.Queue;



/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {

	/*
	 * ATRIBUTOS
	 */
	
	/**
	 * viajes por hora
	 */
	private MaxHeapCP<TravelTime> viajesHora = new MaxHeapCP<>();
	private int trimestreActual;
	private Queue<TravelTime> viajeshora2 = new Queue<TravelTime>();

	/*
	 * METODOS
	 */

	/**
	 * Carga los archivos de los viajes
	 * @param trimestre, trimestre del a�o
	 */
	public void cargar(int trimestre, int limite) {
		if(trimestre > 2 && trimestre < 0){
			System.out.println("Por favor ingrese un valor v�lido de trimestre (entre 1 y 2).");
		} else {
		trimestreActual = trimestre;
		String line = "";
		int sizeMonth = 0;
		int sizehour = 0;
		int sizeweek = 0;
		double menor=9999999;
		double mayor = 0;
		File fileHour = new File("./data/bogota-cadastral-2018-" + trimestre + "-All-HourlyAggregate.csv");
		try (BufferedReader br = new BufferedReader(new FileReader(fileHour))) {
			br.readLine();
			line = br.readLine();
			while (line != null && (limite > 0 ? sizehour < limite : true)) {
				String[] data = line.split(",");
				int dato1 = Integer.parseInt(data[0]);
				int dato2 = Integer.parseInt(data[1]);
				int dato3 = Integer.parseInt(data[2]);
				double dato4 = Double.parseDouble(data[3]);
				double dato5 = Double.parseDouble(data[4]);
				
				TravelTime viaje = new TravelTime(dato1,dato2,dato3,dato4,dato5);
				viajesHora.agregar(viaje);
				viajeshora2.enqueue(viaje);
				sizehour++;
				if(dato1<menor)
				{
					menor=dato1	;
				}
				if(dato2<menor)
				{
					menor=dato2;
				}

				if(dato1>mayor)
				{
					mayor=viaje.getdsId();
				}
				if(dato2>mayor)
				{
					mayor=viaje.getSourceID();
				}
				line = br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Total de viajes en el archivo hora del trimestre "+trimestre+" = "+sizehour+"");
	}
	}

public void generarMuestra(int n)
{
	while(n!=0)
	{
		TravelTime temp = new TravelTime((int)Math.random(),(int)Math.random(), (int)Math.random(),Math.random(),Math.random());
		viajesHora.agregar(temp);
		viajeshora2.enqueue(temp);
		n--;
	}
}





}