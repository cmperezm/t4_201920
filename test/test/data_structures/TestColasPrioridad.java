package test.data_structures;



import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.MaxHeapCP;
import model.data_structures.Queue;
import model.logic.MVCModelo;

public class TestColasPrioridad {

	private MaxHeapCP<Integer> cp;
	private Queue<Integer> cp2;
	private MVCModelo mvc;
	private int tama�o = 1000;
	@Before
	public void setUp1() {
		cp = new MaxHeapCP<>();
		cp2 = new Queue<>();
	while(tama�o>=0)
	{
		cp.agregar(tama�o);
		cp2.enqueue(tama�o);
	}
	}

	public void setUp2() {
		cp=new MaxHeapCP<>();
		cp2= new Queue<>();
	}

	@Test
	public void darNumElementos() {
		
		assertEquals(cp.size(), tama�o);
		assertEquals(cp2.size(), tama�o);
	}

	@Test
	public void testSacarMAx() {
		
		assertEquals(1000, (int)cp.sacarMax());
		assertEquals(cp.size(), tama�o-1);
		
	}
	@Test
	public void testEsVacia()
	{
		setUp2();
		assertEquals(cp.size(), 0);
		assertEquals(cp2.size(), 0);
		
	}
	@Test
	public void darMax()
	{
		assertEquals((int)cp.darMax(), 1000);
		assertEquals(cp.size(), 1000);
	}
	@Test
	public void agregar()
	{
		setUp1();
		cp.agregar(1111);
		assertEquals(cp.size(), tama�o+1);
	}
}
